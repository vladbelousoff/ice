#pragma once

#define ICE_ARRAY_DEFAULT_CAPACITY 17

typedef struct iceArrayT {
   unsigned long size;
   unsigned long capacity;
   unsigned long elemSize;
   char buf[0];
} iceArrayT;

iceArrayT* iceArrayCtor(unsigned long elemSize, unsigned long capacity);
void iceArrayDtor(iceArrayT** self);

unsigned long iceArrayGetSize(iceArrayT* self);
void* iceArrayEmplaceBack(iceArrayT** self);
void* iceArrayGetPtrAt(iceArrayT* self, unsigned long index);
