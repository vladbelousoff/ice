#include "ice_array.h"
#include "ice_common.h"

iceArrayT*
iceArrayCtor(unsigned long elemSize, unsigned long capacity) {
   iceArrayT* self = iceMemInit(sizeof(*self) + capacity * elemSize);
   self->elemSize = elemSize;
   self->capacity = capacity;
   self->size = 0;

   return self;
}

void
iceArrayDtor(iceArrayT** self) {
   iceMemTerm(*self);
   *self = NULL;
}

static iceArrayT*
iceArrayReallocate(iceArrayT* self, unsigned long newCapacity) {
   iceArrayT* newArray = realloc(self, sizeof(*self) + newCapacity * self->elemSize);
   newArray->capacity = newCapacity;
   return newArray;
}

void* iceArrayEmplaceBack(iceArrayT** self) {
   if ((*self)->size == (*self)->capacity) {
      *self = iceArrayReallocate(*self, (*self)->capacity * 2);
   }

   return &(*self)->buf[(*self)->size++ * (*self)->elemSize];
}

unsigned long iceArrayGetSize(iceArrayT* self) {
   return self->size;
}

void* iceArrayGetPtrAt(iceArrayT* self, unsigned long index) {
   assert(index < iceArrayGetSize(self));
   return &self->buf[index * self->elemSize];
}
